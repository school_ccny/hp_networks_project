# ============================================================================
#
#                                 UDP Client
#
# ============================================================================

import os
import sys
import time
import socket
import subprocess
from threading import Thread

from ffmpy.ffmpeg import FFmpeg

HOST = '192.210.231.5'
PORT = 5000
BUFFER_SIZE = 1024

datafile = 'data.txt'
g = open(datafile,'w')
# Create a dgram socket
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
except socket.error:
    print "Couldn't create socket error"
    sys.exit(1)

s.settimeout(1)

def file_transfer():

    wait = 0.009
    chunks = 0
    add = 0.00001

    filepath = os.path.join(os.getcwd(), "data", "big.txt")

    if filepath and os.path.isfile(filepath):
        with open(filepath, 'rb') as f:
            data = f.read(BUFFER_SIZE)
            while data:
                if s.sendto(data, (HOST, PORT)):
                    chunks += 1
                    
                    try:
                        # wait for reply from the server, max 1024 bytes
                        ack, addr = s.recvfrom(BUFFER_SIZE)
                        print ack
                        if ack == 'ok':
                            wait -= add
                            print "accept"
    
                    except socket.timeout:
                    
                        print "fail"
                        wait*=2

                    g.write(str(wait) + ",")
                    data = f.read(BUFFER_SIZE)
                    time.sleep(wait)

        print "Packets sent {0}".format(chunks)
        s.close()
    else:
        print "The given filepath is incorrect"
        sys.exit(1)
"""
def virtual_traffic():

    message = "-1"
    for _ in range(62000):
        s.sendto(message, (HOST, PORT))
        time.sleep(.5)
"""
def run():
    Thread(target=file_transfer).start()
    #Thread(target=virtual_traffic).start()

run()
