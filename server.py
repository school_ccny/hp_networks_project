# ============================================================================
#
#                                UDP server
#
# ============================================================================

import sys
import socket

# HOST is left empty, it's symbolic meaning all availble internfaces
HOST = ''
PORT = 5000
BUFFER_SIZE = 1024

#  Datagram Socket (UDP)
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Accept up to incoming connections
    print "Socket Created!"
except socket.err, msg:
    print "Couldn't create UDP socket. Error code: {0} Message: {1}".format(
        str(msg[0]), msg[1])
    sys.exit(1)

# Bind socket to local host and port
try:
    s.bind((HOST, PORT))
except socket.err, msg:
    print "Unable to bind. Error Code: {0} Message: {1}".format(
        str(msg[0]), msg[1])
    sys.exit(1)

print "Socket binding completed"


while True:
    
    data, addr = s.recvfrom(BUFFER_SIZE)
    with open("test.txt", 'wb') as f:

        while(data):
            
            if data == "-1":
                continue
            else:
                print "Receiving File ..."
                f.write(data)

                # Send ack to client
                s.sendto("ok", addr)

            data, addr = s.recvfrom(BUFFER_SIZE)
        s.close()
            
        print "File Downloaded"
