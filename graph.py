import matplotlib.pyplot as plt

with open('data.txt','r') as f:
	raw = f.read()

data = raw.strip('').split(',')
data = map(float,data[:-1])

x = range(len(data))

plt.figure()
plt.plot(x,data)
plt.title("Packet Send Rate Over Time")
plt.xlabel('Packets Sent')
plt.ylabel('Time to send packet (sec)')
plt.savefig("final_graph.png")